﻿using HJMinimally.Minio.IServices;
using HJMinimally.Minio.Model;
using HJMinimally.Minio.ViewModel.Req;
using HJMinimally.Utility.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HJMinimally.Minio.WebApi.Controllers
{
    /// <summary>
    /// 系统上传配置
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class FileUploadConfigController : ControllerBase
    {
        private readonly IFileUploadConfigService _fileUploadConfigService;
        /// <summary>
        /// 初始化构造函数
        /// </summary>
        /// <param name="fileUploadConfigService"></param>
        public FileUploadConfigController(IFileUploadConfigService fileUploadConfigService)
        {
            _fileUploadConfigService = fileUploadConfigService;
        }
        /// <summary>
        /// 获取系统上传配置列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<Response<List<FileUploadConfigEntity>>> GetFileUploadConfigList()
        {
            var result = new Response<List<FileUploadConfigEntity>>();
            result.data = await _fileUploadConfigService.GetFileUploadConfigList();
            return result;
        }
        /// <summary>
        /// 根据appid获取系统上传配置信息
        /// </summary>
        /// <param name="req">输入参数</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<Response<FileUploadConfigEntity>> GetFileUploadConfigByAppId([FromQuery] GetFileUploadConfigByAppIdReq req)
        {
            var result = new Response<FileUploadConfigEntity>();
            result.data = await _fileUploadConfigService.GetFileUploadConfigByAppId(req.appid);
            return result;
        }
        /// <summary>
        /// 根据主键获取系统上传配置信息
        /// </summary>
        /// <param name="req">输入参数</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<Response<FileUploadConfigEntity>> GetFileUploadConfigById([FromQuery] GetFileUploadConfigByIdReq req)
        {
            var result = new Response<FileUploadConfigEntity>();
            result.data = await _fileUploadConfigService.GetFileUploadConfigById(req.id);
            return result;
        }
        /// <summary>
        /// 查看常用格式支持
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<Response<List<FileFormatEntity>>> GetFileFormatList()
        {
            var result = new Response<List<FileFormatEntity>>();
            result.data = await _fileUploadConfigService.GetFileFormatList();
            return result;
        }
        /// <summary>
        /// 保存系统上传配置表单（新增、修改）
        /// </summary>
        /// <param name="req">输入参数</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<Response<bool>> SaveFileUploadConfigForm([FromBody] SaveFileUploadConfigFormReq req)
        {
            return await _fileUploadConfigService.SaveFileUploadConfigForm(req);
        }
        /// <summary>
        /// 根据系统Appid删除系统上传配置信息
        /// </summary>
        /// <param name="req">输入参数</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<Response<bool>> DeleteFileUploadConfigByAppId([FromBody] DeleteFileUploadConfigByAppIdReq req)
        {
            return await _fileUploadConfigService.DeleteFileUploadConfigByAppId(req.appid);
        }
        /// <summary>
        /// 根据主键删除系统上传配置信息
        /// </summary>
        /// <param name="req">输入参数</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<Response<bool>> DeleteFileUploadConfigById([FromBody] DeleteFileUploadConfigByIdReq req)
        {
            return await _fileUploadConfigService.DeleteFileUploadConfigById(req.id);
        }
    }
}
