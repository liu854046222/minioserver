﻿using HJMinimally.Minio.IServices;
using HJMinimally.Minio.Model;
using HJMinimally.Minio.ViewModel.Req;
using HJMinimally.Minio.ViewModel.Resp;
using HJMinimally.Utility.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using Minio;
using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace HJMinimally.Minio.WebApi.Controllers
{
    /// <summary>
    /// 文件上传
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class FileUploadController : ControllerBase
    {
        private readonly MinioConfigEntity _minioConfigEntity;
        private readonly IFileUploadService _fileUploadService;
        /// <summary>
        /// 初始化构造函数
        /// </summary>
        /// <param name="fileUploadService"></param>
        /// <param name="minioConfigService"></param>
        public FileUploadController(IFileUploadService fileUploadService, IMinioConfigService minioConfigService)
        {
            _fileUploadService = fileUploadService;
            _minioConfigEntity = minioConfigService.GetMinioConfig().Result;
        }
        /// <summary>
        /// 上传文件
        /// </summary>
        /// <param name="req">输入参数</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<Response<UploadFileResp>> UploadFile([FromForm] UploadFileReq req)
        {
            return await _fileUploadService.UploadFile(req);
        }
        /// <summary>
        /// 下载服务器文件
        /// </summary>
        /// <param name="req">输入参数</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> DownloadFileById([FromBody] DownloadFileByIdReq req)
        {
            var entity = await _fileUploadService.GetFileUploadEntity(req.id);
            var ext = Path.GetExtension(entity.filepath).ToLower();
            //获取文件的ContentType
            var provider = new FileExtensionContentTypeProvider();
            var mime = provider.Mappings[ext];
            var path = _minioConfigEntity.serverurl + $"/" + entity.appid + $"/" + entity.filepath;
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(path);
            var stream = await client.GetStreamAsync(path);
            return File(stream, mime, Path.GetFileName(path));
        }
        /// <summary>
        /// 下载服务器文件
        /// </summary>
        /// <param name="req">输入参数</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> DownloadFileByPath([FromBody] DownloadFileByPathReq req)
        {
            var ext = Path.GetExtension(req.filepath).ToLower();
            //获取文件的ContentType
            var provider = new FileExtensionContentTypeProvider();
            var mime = provider.Mappings[ext];
            var path = _minioConfigEntity.serverurl + $"/" + req.appid + $"/" + req.filepath;
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(path);
            var stream = await client.GetStreamAsync(path);
            return File(stream, mime, Path.GetFileName(path));
        }
        /// <summary>
        /// 下载网络文件到本地
        /// </summary>
        /// <param name="req">输入参数</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> DownloadFileByUrl([FromBody] DownloadFileByUrlReq req)
        {
            var ext = Path.GetExtension(req.fileurl).ToLower();
            //获取文件的ContentType
            var provider = new FileExtensionContentTypeProvider();
            var mime = provider.Mappings[ext];
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(req.fileurl);
            var stream = await client.GetStreamAsync(req.fileurl);
            return File(stream, mime, Path.GetFileName(req.fileurl));
        }
        /// <summary>
        /// 删除服务器文件
        /// </summary>
        /// <param name="req">输入参数</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<Response<bool>> DeleteFileById([FromBody] DeleteFileByIdReq req)
        {
            var entity = await _fileUploadService.GetFileUploadEntity(req.id);
            if (!string.IsNullOrEmpty(entity.thumbnail))
            {
                var thumbnailReq = new DeleteFileByPathReq
                {
                    appid = entity.appid,
                    filepath = entity.thumbnail
                };
                await _fileUploadService.DeleteFileByPath(thumbnailReq);
            }
            return await _fileUploadService.DeleteFileById(req);
        }
        /// <summary>
        /// 删除服务器文件
        /// </summary>
        /// <param name="req">输入参数</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<Response<bool>> DeleteFileByPath([FromBody] DeleteFileByPathReq req)
        {
            return await _fileUploadService.DeleteFileByPath(req);
        }
    }
}
