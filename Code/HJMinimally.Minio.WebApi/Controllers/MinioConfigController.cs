﻿using HJMinimally.Minio.IServices;
using HJMinimally.Minio.Model;
using HJMinimally.Minio.ViewModel.Req;
using HJMinimally.Utility.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HJMinimally.Minio.WebApi.Controllers
{
    /// <summary>
    /// Minio文件服务配置
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class MinioConfigController : ControllerBase
    {
        private readonly IMinioConfigService _minioConfigService;
        /// <summary>
        /// 初始化构造函数
        /// </summary>
        /// <param name="minioConfigService"></param>
        public MinioConfigController(IMinioConfigService minioConfigService)
        {
            _minioConfigService = minioConfigService;
        }
        /// <summary>
        /// 获取Minio文件服务配置
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<Response<MinioConfigEntity>> GetMinioConfig()
        {
            var result = new Response<MinioConfigEntity>();
            result.data = await _minioConfigService.GetMinioConfig();
            return result;
        }
        /// <summary>
        /// 保存Minio文件服务配置表单
        /// </summary>
        /// <param name="req">输入参数</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<Response<bool>> SaveMinioConfigForm([FromBody] SaveMinioConfigFormReq req)
        {
            return await _minioConfigService.SaveMinioConfigForm(req);
        }
    }
}
