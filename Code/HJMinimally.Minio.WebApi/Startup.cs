using HJMinimally.AutoMapper;
using HJMinimally.Minio.WebApi.Filter;
using HJMinimally.Repository;
using HJMinimally.ServiceDiscovery.Consul.Extensions;
using HJMinimally.ServiceDiscovery.Extensions;
using HJMinimally.Swagger;
using HJMinimally.Utility.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Reflection;

namespace HJMinimally.Minio.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHJMinimally3_1(builder =>
            {
                builder.AddAspNetCore();
                builder.AddHJSwagger();
                builder.AddHJAutoMapper();
                // 添加数据ORM、数据仓储
                builder.AddSqlSugarDbContext().AddSqlSugarDbRepository();
                //添加服务发现
                builder.AddServiceDiscovery3_1(tt => { tt.UseConsul(); });
                // 添加应用批量注册
                Assembly[] assemblies = new Assembly[] {
                    Assembly.Load("HJMinimally.Minio.Services")
                };
                builder.BatchRegisterService(assemblies, ServiceLifetime.Scoped);
            });
            services.AddHealthChecks();
            services.AddControllers(config =>
            {
                config.Filters.Add<GlobalAuthFilter>();//注入全局验证
                config.Filters.Add(typeof(GlobalExceptionFilter)); //注入全局异常处理
            });
           
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseHJSwagger();
            app.UseHJAutoMapper();
            //服务注册
            app.UseConsulRegisterService(Configuration);
            //健康检查
            app.UseHealthChecks("/health");
            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
