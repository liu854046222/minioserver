﻿using HJMinimally.Minio.Model;
using HJMinimally.Minio.ViewModel.Req;
using HJMinimally.Utility.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HJMinimally.Minio.IServices
{
    /// <summary>
    /// Minio文件服务配置接口
    /// </summary>
    public interface IMinioConfigService
    {
        /// <summary>
        /// 获取Minio文件服务配置
        /// </summary>
        /// <returns></returns>
        Task<MinioConfigEntity> GetMinioConfig();
        /// <summary>
        /// 保存Minio文件服务配置表单
        /// </summary>
        /// <param name="req">输入参数</param>
        /// <returns></returns>
        Task<Response<bool>> SaveMinioConfigForm(SaveMinioConfigFormReq req);
    }
}
