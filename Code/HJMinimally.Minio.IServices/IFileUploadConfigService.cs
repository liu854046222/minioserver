﻿using HJMinimally.Minio.Model;
using HJMinimally.Minio.ViewModel.Req;
using HJMinimally.Utility.Http;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HJMinimally.Minio.IServices
{
    /// <summary>
    /// 系统上传配置接口服务
    /// </summary>
    public interface IFileUploadConfigService
    {
        #region 获取数据
        /// <summary>
        /// 获取系统上传配置列表
        /// </summary>
        /// <returns></returns>
        Task<List<FileUploadConfigEntity>> GetFileUploadConfigList();
        /// <summary>
        /// 根据appid获取系统上传配置信息
        /// </summary>
        /// <param name="appid">系统appid</param>
        /// <returns></returns>
        Task<FileUploadConfigEntity> GetFileUploadConfigByAppId(string appid);
        /// <summary>
        /// 根据主键获取系统上传配置信息
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        Task<FileUploadConfigEntity> GetFileUploadConfigById(string id);
        /// <summary>
        /// 常用格式支持
        /// </summary>
        /// <returns></returns>
        Task<List<FileFormatEntity>> GetFileFormatList();
        #endregion
        #region 数据提交
        /// <summary>
        /// 保存系统上传配置表单（新增、修改）
        /// </summary>
        /// <param name="req">系统上传配置输入实体</param>
        /// <returns></returns>
        Task<Response<bool>> SaveFileUploadConfigForm(SaveFileUploadConfigFormReq req);
        /// <summary>
        /// 根据系统Appid删除系统上传配置信息
        /// </summary>
        /// <param name="appid">系统appid</param>
        /// <returns></returns>
        Task<Response<bool>> DeleteFileUploadConfigByAppId(string appid);
        /// <summary>
        /// 根据主键删除系统上传配置信息
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        Task<Response<bool>> DeleteFileUploadConfigById(string id);
        #endregion
    }
}
