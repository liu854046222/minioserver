﻿using HJMinimally.Minio.Model;
using HJMinimally.Minio.ViewModel.Req;
using HJMinimally.Minio.ViewModel.Resp;
using HJMinimally.Utility.Http;
using System.IO;
using System.Threading.Tasks;

namespace HJMinimally.Minio.IServices
{
    /// <summary>
    /// 文件上传记录接口服务
    /// </summary>
    public interface IFileUploadService
    {
        /// <summary>
        /// 上传文件
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        Task<Response<UploadFileResp>> UploadFile(UploadFileReq req);
        /// <summary>
        /// 获取文件信息
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        Task<FileUploadEntity> GetFileUploadEntity(string id);
        /// <summary>
        /// 删除服务器文件
        /// </summary>
        /// <param name="req">输入参数</param>
        /// <returns></returns>
        Task<Response<bool>> DeleteFileById(DeleteFileByIdReq req);
        /// <summary>
        /// 删除服务器文件
        /// </summary>
        /// <param name="req">输入参数</param>
        /// <returns></returns>
        Task<Response<bool>> DeleteFileByPath(DeleteFileByPathReq req);
    }
}
