﻿using SqlSugar;
using System.ComponentModel;

namespace HJMinimally.Minio.Model
{
    /// <summary>
    /// Minio文件服务配置
    /// </summary>
    [SugarTable("minioconfig")]
    public class MinioConfigEntity
    {
        #region 实体
        /// <summary>
        /// 主键
        /// </summary>
        [SugarColumn(ColumnName = "id", IsPrimaryKey = true)]
        [Description("主键")]
        public string id { get; set; }

        /// <summary>
        /// Minio服务地址，不带http
        /// </summary>
        [SugarColumn(ColumnName = "endpoint")]
        [Description("Minio服务地址，不带http")]
        public string endpoint { get; set; }

        /// <summary>
        /// Minio服务AccessKey
        /// </summary>
        [SugarColumn(ColumnName = "accesskey")]
        [Description("Minio服务AccessKey")]
        public string accesskey { get; set; }

        /// <summary>
        /// Minio服务SecretKey
        /// </summary>
        [SugarColumn(ColumnName = "secretkey")]
        [Description("Minio服务SecretKey")]
        public string secretkey { get; set; }

        /// <summary>
        /// Minio服务地址，带http
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public string serverurl
        {
            get
            {
                var result = "";
                if (!string.IsNullOrEmpty(endpoint))
                {
                    result = endpoint.Contains("http") ? endpoint : "http://" + endpoint;
                }
                return result;
            }
        }
        #endregion
    }
}
