﻿using SqlSugar;
using System;
using System.ComponentModel;

namespace HJMinimally.Minio.Model
{
    /// <summary>
    /// 文件上传记录
    /// </summary>
    [SugarTable("fileupload")]
    public class FileUploadEntity
    {
        #region 实体
        /// <summary>
        /// 主键
        /// </summary>
        [SugarColumn(ColumnName = "fileid", IsPrimaryKey = true)]
        [Description("主键")]
        public string fileid { get; set; }

        /// <summary>
        /// 文件名称
        /// </summary>
        [SugarColumn(ColumnName = "filename")]
        [Description("文件名称")]
        public string filename { get; set; }

        /// <summary>
        /// 文件地址
        /// </summary>
        [SugarColumn(ColumnName = "filepath")]
        [Description("文件地址")]
        public string filepath { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [SugarColumn(ColumnName = "description")]
        [Description("描述")]
        public string description { get; set; }

        /// <summary>
        /// 文件大小
        /// </summary>
        [SugarColumn(ColumnName = "filesize")]
        [Description("文件大小")]
        public int? filesize { get; set; }

        /// <summary>
        /// 文件扩展名(eg:jpg,png)
        /// </summary>
        [SugarColumn(ColumnName = "extension")]
        [Description("文件扩展名(eg:jpg,png)")]
        public string extension { get; set; }

        /// <summary>
        /// 文件类型（默认是&quot;application/octet-stream&quot;）
        /// </summary>
        [SugarColumn(ColumnName = "contentype")]
        [Description("文件类型（默认是application/octet-stream）")]
        public string contentype { get; set; }

        /// <summary>
        /// 缩略图
        /// </summary>
        [SugarColumn(ColumnName = "thumbnail")]
        [Description("缩略图")]
        public string thumbnail { get; set; }

        /// <summary>
        /// 系统appid
        /// </summary>
        [SugarColumn(ColumnName = "appid")]
        [Description("系统appid")]
        public string appid { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [SugarColumn(ColumnName = "createtime")]
        [Description("创建时间")]
        public DateTime? createtime { get; set; }

        #endregion
    }
}
