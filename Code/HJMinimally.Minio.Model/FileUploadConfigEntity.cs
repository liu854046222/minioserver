﻿using SqlSugar;
using System;
using System.ComponentModel;

namespace HJMinimally.Minio.Model
{
    /// <summary>
    /// 系统上传配置表
    /// </summary>
    [SugarTable("fileuploadconfig")]
    public class FileUploadConfigEntity
    {
        #region 实体
        /// <summary>
        /// 主键
        /// </summary>
        [SugarColumn(ColumnName = "id", IsPrimaryKey = true)]
        [Description("主键")]
        public string id { get; set; }

        /// <summary>
        /// 系统Appid
        /// </summary>
        [SugarColumn(ColumnName = "appid")]
        [Description("系统Appid")]
        public string appid { get; set; }

        /// <summary>
        /// 图片上传类型
        /// </summary>
        [SugarColumn(ColumnName = "imgextension")]
        [Description("图片上传类型")]
        public string imgextension { get; set; }

        /// <summary>
        /// 视频上传类型
        /// </summary>
        [SugarColumn(ColumnName = "videoextension")]
        [Description("视频上传类型")]
        public string videoextension { get; set; }

        /// <summary>
        /// 其它文件上传类型
        /// </summary>
        [SugarColumn(ColumnName = "fileextension")]
        [Description("其它文件上传类型")]
        public string fileextension { get; set; }

        /// <summary>
        /// 非法文件上传类型
        /// </summary>
        [SugarColumn(ColumnName = "illegalextension")]
        [Description("非法文件上传类型")]
        public string illegalextension { get; set; }

        /// <summary>
        /// 图片上传大小(KB)
        /// </summary>
        [SugarColumn(ColumnName = "imgsize")]
        [Description("图片上传大小(KB)")]
        public int? imgsize { get; set; }

        /// <summary>
        /// 视频上传大小(KB)
        /// </summary>
        [SugarColumn(ColumnName = "videosize")]
        [Description("视频上传大小(KB)")]
        public int? videosize { get; set; }

        /// <summary>
        /// 其它文件上传大小(KB)
        /// </summary>
        [SugarColumn(ColumnName = "filesize")]
        [Description("其它文件上传大小(KB)")]
        public int? filesize { get; set; }

        /// <summary>
        /// 图片最大高度(像素)
        /// </summary>
        [SugarColumn(ColumnName = "imgmaxheight")]
        [Description("图片最大高度(像素)")]
        public int? imgmaxheight { get; set; }

        /// <summary>
        /// 图片最大宽度(像素)
        /// </summary>
        [SugarColumn(ColumnName = "imgmaxwidth")]
        [Description("图片最大宽度(像素)")]
        public int? imgmaxwidth { get; set; }

        /// <summary>
        /// 是否启用缩略图（0：不启用；1：启用）
        /// </summary>
        [SugarColumn(ColumnName = "isthumbnail")]
        [Description("是否启用缩略图（0：不启用；1：启用）")]
        public int? isthumbnail { get; set; }

        /// <summary>
        /// 生成缩略图高度(像素)
        /// </summary>
        [SugarColumn(ColumnName = "thumbnailheight")]
        [Description("生成缩略图高度(像素)")]
        public int? thumbnailheight { get; set; }

        /// <summary>
        /// 生成缩略图宽度(像素)
        /// </summary>
        [SugarColumn(ColumnName = "thumbnailwidth")]
        [Description("生成缩略图宽度(像素)")]
        public int? thumbnailwidth { get; set; }

        /// <summary>
        /// 缩略图生成方式（Cut：裁剪，HW：补白）
        /// </summary>
        [SugarColumn(ColumnName = "thumbnailmode")]
        [Description("缩略图生成方式（Cut：裁剪，HW：补白）")]
        public string thumbnailmode { get; set; }

        /// <summary>
        /// 文件存储格式（0：按年/月/日存入不同目录；1：按年月/日/存入不同目录；2：按年月日每天一个目录）
        /// </summary>
        [SugarColumn(ColumnName = "filesaveformat")]
        [Description("文件存储格式（0：按年/月/日存入不同目录；1：按年月/日/存入不同目录；2：按年月日每天一个目录）")]
        public int? filesaveformat { get; set; }

        #endregion
    }
}
