﻿using SqlSugar;
using System.ComponentModel;

namespace HJMinimally.Minio.Model
{
    /// <summary>
    /// 常用格式支持
    /// </summary>
    [SugarTable("fileformat")]
    public class FileFormatEntity
    {
        #region 实体
        /// <summary>
        /// 主键
        /// </summary>
        [SugarColumn(ColumnName = "id", IsPrimaryKey = true)]
        [Description("主键")]
        public string id { get; set; }

        /// <summary>
        /// 格式名称
        /// </summary>
        [SugarColumn(ColumnName = "formatname")]
        [Description("格式名称")]
        public string formatname { get; set; }

        /// <summary>
        /// 支持格式
        /// </summary>
        [SugarColumn(ColumnName = "formatvalue")]
        [Description("支持格式")]
        public string formatvalue { get; set; }

        #endregion
    }
}
