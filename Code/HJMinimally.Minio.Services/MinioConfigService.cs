﻿using HJMinimally.AutoMapper;
using HJMinimally.Cache.Redis;
using HJMinimally.Minio.IServices;
using HJMinimally.Minio.Model;
using HJMinimally.Minio.ViewModel.Req;
using HJMinimally.Repository.SqlSugar;
using HJMinimally.Utility;
using HJMinimally.Utility.Http;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace HJMinimally.Minio.Services
{
    /// <summary>
    /// Minio文件服务配置
    /// </summary>
    public class MinioConfigService : IMinioConfigService
    {
        private readonly string cacheKey = "hjminimally_minio_minioconfig";
        private readonly ISqlSugarDbRepository _app;
        public MinioConfigService(ISqlSugarDbRepository app)
        {
            _app = app;
        }
        /// <summary>
        /// 获取Minio文件服务配置
        /// </summary>
        /// <returns></returns>
        public async Task<MinioConfigEntity> GetMinioConfig()
        {
            try
            {

                var entity = RedisCache.Read<MinioConfigEntity>(cacheKey, CacheId.db0);
                if (entity == null)
                {
                    entity = (await _app.GetListAsync<MinioConfigEntity>()).FirstOrDefault();
                    RedisCache.Write(cacheKey, entity, CacheId.db0);
                }
                return entity;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 保存Minio文件服务配置表单
        /// </summary>
        /// <param name="req">输入参数</param>
        /// <returns></returns>
        public async Task<Response<bool>> SaveMinioConfigForm(SaveMinioConfigFormReq req)
        {
            try
            {
                var result = new Response<bool>();
                RedisCache.Remove(cacheKey, CacheId.db0);
                var entity = req.MapTo<MinioConfigEntity>();
                if (string.IsNullOrEmpty(entity.id))
                {
                    var _entity = await GetMinioConfig();
                    if (_entity.endpoint == req.endpoint)
                    {
                        result.info = "该配置已存在";
                        result.data = false;
                        return result;
                    }
                    entity.id = Guid.NewGuid().ToString();
                    await _app.InsertAsync(entity);
                }
                else
                {
                    await _app.UpdateIgnoreColumnsAsync(entity);
                }
                result.data = true;
                return result;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
    }
}
