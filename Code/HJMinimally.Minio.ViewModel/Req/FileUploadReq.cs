﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace HJMinimally.Minio.ViewModel.Req
{
    /// <summary>
    /// 文件上传输入模型
    /// </summary>
    public class UploadFileReq
    {
        /// <summary>
        /// 文件
        /// </summary>
        public IFormFile file { get; set; }
        /// <summary>
        /// 所属系统appid
        /// </summary>
        [Required(ErrorMessage = "所属系统appid不能为空")]
        public string appid { get; set; }
    }
    /// <summary>
    /// 下载文件输入模型
    /// </summary>
    public class DownloadFileByIdReq
    {
        /// <summary>
        /// 文件主键
        /// </summary>
        [Required(ErrorMessage = "文件主键不能为空")]
        public string id { get; set; }
    }
    /// <summary>
    /// 下载文件输入模型
    /// </summary>
    public class DownloadFileByPathReq
    {
        /// <summary>
        /// 所属系统appid
        /// </summary>
        [Required(ErrorMessage = "所属系统appid不能为空")]
        public string appid { get; set; }
        /// <summary>
        /// 文件相对路径地址，前面不带‘/’,如2021/02/08/2021020816251285940188.png
        /// </summary>
        [Required(ErrorMessage = "文件地址不能为空")]
        public string filepath { get; set; }
    }
    /// <summary>
    /// 下载文件输入模型
    /// </summary>
    public class DownloadFileByUrlReq
    {
        /// <summary>
        /// 文件网络地址，前面不带‘/’,如http://127.0.0.1/baseinfo/2021/02/08/2021020816251285940188.png
        /// </summary>
        [Required(ErrorMessage = "文件网络地址不能为空")]
        public string fileurl { get; set; }
    }
    /// <summary>
    /// 删除服务器文件输入模型
    /// </summary>
    public class DeleteFileByIdReq
    {
        /// <summary>
        /// 文件主键
        /// </summary>
        [Required(ErrorMessage = "文件主键不能为空")]
        public string id { get; set; }
    }
    /// <summary>
    /// 删除服务器文件输入模型
    /// </summary>
    public class DeleteFileByPathReq
    {
        /// <summary>
        /// 所属系统appid
        /// </summary>
        [Required(ErrorMessage = "所属系统appid不能为空")]
        public string appid { get; set; }
        /// <summary>
        /// 文件相对路径地址，前面不带‘/’,如2021/02/08/2021020816251285940188.png
        /// </summary>
        [Required(ErrorMessage = "文件地址不能为空")]
        public string filepath { get; set; }
    }
}
