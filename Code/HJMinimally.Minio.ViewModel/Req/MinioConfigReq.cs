﻿using System.ComponentModel.DataAnnotations;

namespace HJMinimally.Minio.ViewModel.Req
{
    /// <summary>
    /// 保存Minio文件服务配置表单输入模型
    /// </summary>
    public class SaveMinioConfigFormReq
    {
        /// <summary>
        /// 主键
        /// </summary>
        public string id { get; set; }

        /// <summary>
        /// Minio服务地址，不带http
        /// </summary>
        [Required(ErrorMessage = "Minio服务地址不为空")]
        public string endpoint { get; set; }

        /// <summary>
        /// Minio服务AccessKey
        /// </summary>
        [Required(ErrorMessage = "Minio服务AccessKey不为空")]
        public string accesskey { get; set; }

        /// <summary>
        /// Minio服务SecretKey
        /// </summary>
        [Required(ErrorMessage = "Minio服务SecretKey不为空")]
        public string secretkey { get; set; }
    }
}
