﻿using System.ComponentModel.DataAnnotations;

namespace HJMinimally.Minio.ViewModel.Req
{
    /// <summary>
    /// 根据appid获取系统上传配置信息输入模型
    /// </summary>
    public class GetFileUploadConfigByAppIdReq
    {
        /// <summary>
        /// 系统Appid
        /// </summary>
        [Required(ErrorMessage = "系统Appid不为空")]
        public string appid { get; set; }
    }
    /// <summary>
    /// 根据主键获取系统上传配置信息输入模型
    /// </summary>
    public class GetFileUploadConfigByIdReq
    {
        /// <summary>
        /// 主键
        /// </summary>
        [Required(ErrorMessage = "主键不为空")]
        public string id { get; set; }
    }
    /// <summary>
    /// 保存系统上传配置表单输入模型
    /// </summary>
    public class SaveFileUploadConfigFormReq
    {
        /// <summary>
        /// 主键
        /// </summary>
        public string id { get; set; }

        /// <summary>
        /// 系统Appid
        /// </summary>
        [Required(ErrorMessage = "系统Appid不为空")]
        public string appid { get; set; }

        /// <summary>
        /// 图片上传类型
        /// </summary>
        [Required(ErrorMessage = "图片上传类型不为空")]
        public string imgextension { get; set; } = "bmp,gif,jpeg,jpg,png,tif,tiff";

        /// <summary>
        /// 视频上传类型
        /// </summary>
        [Required(ErrorMessage = "视频上传类型不为空")]
        public string videoextension { get; set; } = "3gp,asf,avi,flv,m4v,mkv,mov,mp4,mpeg,mpg,rm,rmvb,wmv";

        /// <summary>
        /// 其它文件上传类型
        /// </summary>
        [Required(ErrorMessage = "其它文件上传类型不为空")]
        public string fileextension { get; set; } = "doc,docm,docx,xls,xlsb,xlsm,xlsx,pps,ppsm,ppsx,ppt,pptm,pptx,pdf,csv,rtf,txt,7z,rar,zip";

        /// <summary>
        /// 非法文件上传类型
        /// </summary>
        [Required(ErrorMessage = "非法文件上传类型不为空")]
        public string illegalextension { get; set; } = "asp,aspx,bat,c,coffee,cpp,cs,css,go,htm,html,java,js,json,jsp,less,lua,m,php,pl,py,rb,sass,scss,sql,swift,ts,vb,vbs,xml,bat";

        /// <summary>
        /// 图片上传大小(KB)
        /// </summary>
        [Required(ErrorMessage = "图片上传大小不为空")]
        public int? imgsize { get; set; } = 5120;

        /// <summary>
        /// 视频上传大小(KB)
        /// </summary>
        [Required(ErrorMessage = "视频上传大小不为空")]
        public int? videosize { get; set; } = 10240;

        /// <summary>
        /// 其它文件上传大小(KB)
        /// </summary>
        [Required(ErrorMessage = "其它文件上传大小不为空")]
        public int? filesize { get; set; } = 10240;

        /// <summary>
        /// 图片最大高度(像素)
        /// </summary>
        [Required(ErrorMessage = "图片最大高度不为空")]
        public int? imgmaxheight { get; set; } = 1920;

        /// <summary>
        /// 图片最大宽度(像素)
        /// </summary>
        [Required(ErrorMessage = "图片最大宽度不为空")]
        public int? imgmaxwidth { get; set; } = 1080;

        /// <summary>
        /// 是否启用缩略图（0：不启用；1：启用）
        /// </summary>
        [Required(ErrorMessage = "是否启用缩略图不为空")]
        public int? isthumbnail { get; set; } = 1;

        /// <summary>
        /// 生成缩略图高度(像素)
        /// </summary>
        [Required(ErrorMessage = "生成缩略图高度不为空")]
        public int? thumbnailheight { get; set; } = 300;

        /// <summary>
        /// 生成缩略图宽度(像素)
        /// </summary>
        [Required(ErrorMessage = "生成缩略图宽度不为空")]
        public int? thumbnailwidth { get; set; } = 300;

        /// <summary>
        /// 缩略图生成方式（Cut：裁剪，HW：补白）
        /// </summary>
        [Required(ErrorMessage = "缩略图生成方式不为空")]
        public string thumbnailmode { get; set; } = "Cut";

        /// <summary>
        /// 文件存储格式（0：按年/月/日存入不同目录；1：按年月/日/存入不同目录；2：按年月日每天一个目录）
        /// </summary>
        [Required(ErrorMessage = "文件存储格式不为空")]
        public int? filesaveformat { get; set; } = 0;
    }
    /// <summary>
    /// 根据系统Appid删除系统上传配置信息输入模型
    /// </summary>
    public class DeleteFileUploadConfigByAppIdReq
    {
        /// <summary>
        /// 系统Appid
        /// </summary>
        [Required(ErrorMessage = "系统Appid不为空")]
        public string appid { get; set; }
    }
    /// <summary>
    /// 根据主键删除系统上传配置信息输入模型
    /// </summary>
    public class DeleteFileUploadConfigByIdReq
    {
        /// <summary>
        /// 主键
        /// </summary>
        [Required(ErrorMessage = "主键不为空")]
        public string id { get; set; }
    }
}
