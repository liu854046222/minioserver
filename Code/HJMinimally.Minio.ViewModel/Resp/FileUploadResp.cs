﻿using HJMinimally.Minio.Model;

namespace HJMinimally.Minio.ViewModel.Resp
{
    /// <summary>
    /// 文件上传输出模型
    /// </summary>
    public class UploadFileResp : FileUploadEntity
    {
        /// <summary>
        /// 文件网络地址
        /// </summary>
        public string fileurl { get; set; }
        /// <summary>
        /// 缩略图网络地址，如果无则为空
        /// </summary>
        public string thumbnailurl { get; set; }
    }
}
