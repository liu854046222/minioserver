echo off
::验证当前是否使用管理员身份运行
>nul 2>&1 "%SYSTEMROOT%\system32\cacls.exe" "%SYSTEMROOT%\system32\config\system"
if '%errorlevel%' EQU '0' (
    echo 当前脚本正在使用管理员身份运行√
) else (
    echo 当前不是以管理员身份运行，请右键使用管理员身份运行！！！
    pause
    exit
)
::切换到当前目录
cd  %~dp0

::说明及注意事项
echo 作者：网络编程
echo 邮箱：854046222@qq.com
echo 日期：2021-03-09
echo Nssm官网地址：http://www.nssm.cc/usage
echo 请注意！请注意！请注意！（重要的事说三遍）：
echo 输入服务数据库的实际程序本身是 nssm，因此在安装服务后【不得移动或删除nssm.exe】
echo 如果您确实希望更改nssm.exe的路径，可以删除并重新安装该服务。
::输出选项菜单
:Menu
echo.
::设置控制台背景色
Color 2f
echo ***************Minio对象存储服务*********
echo ***************1：安装服务***************
echo ***************2: 删除服务***************
echo ***************3: 启动服务***************
echo ***************4: 停止服务***************
echo ***************5: 重启服务***************
echo ***************6: 退    出***************
set /p select=请输入数字，按回车（Enter）键继续：

if %select%==1 (
  goto InstallServer
) 
if %select%==2 (
  goto RemoveServer
) 
if %select%==3 (
  goto StartServer
) 
if %select%==4 (
  goto StopServer
) 
if %select%==5 (
  goto RestartServer
) 
if %select%==6 (
  exit
) 
ELSE (
  echo 输入不正确的数字
  goto Menu
)
::安装服务
:InstallServer
::minio.exe文件路径
set MinioPath="D:\minio\minio.exe"
::执行安装服务指令
nssm install MinioServer "%MinioPath%"
REM "%MinioPath%"
::应用程序配置Tab
nssm set MinioServer Application "%MinioPath%"
nssm set MinioServer AppDirectory "D:\minio"
nssm set MinioServer AppParameters server D:\minio\data
::监控日志
nssm set MinioServer AppStdout D:\minio\Log\Info.log
nssm set MinioServer AppStderr D:\minio\Log\Error.log
::详情Tab（例如：服务的描述信息）
nssm set MinioServer DisplayName MinioServer
nssm set MinioServer Description Minio对象存储服务
nssm set MinioServer Start SERVICE_AUTO_START
::登陆选项Tab(使用本地Administrator登陆，注：必须有./表示本地的用户，默认LOCALSYSTEM)
::nssm set BW_Wexflow ObjectName ".\Administrator" "bwgateway123@"
::安装完成后，启动前的服务状态
nssm status MinioServer
nssm start MinioServer
::启动服务后的服务状态
nssm status MinioServer
goto Menu

::删除服务
:RemoveServer
nssm stop MinioServer
nssm remove MinioServer
goto Menu

::启动服务
:StartServer
nssm start MinioServer
nssm status MinioServer
goto Menu

::停止服务
:StopServer
nssm stop MinioServer
nssm status MinioServer
goto Menu

::重启服务
:RestartServer
nssm restart MinioServer
nssm status MinioServer
goto Menu
